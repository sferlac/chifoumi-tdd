<?php

namespace App;

use App\Gestes\Geste;

class Arbitre
{
    const RESULTAT_EGALITE = "égalité";

    public function juge(Geste $gesteJoueur1, Geste $gesteJoueur2)
    {
        if($gesteJoueur1->gagneContre($gesteJoueur2)){
            return $gesteJoueur1->intitule();
        }else{
            if($gesteJoueur1->egale($gesteJoueur2)){
                return self::RESULTAT_EGALITE;
            }
        }
        return $gesteJoueur2->intitule();
    }
}