<?php

namespace App;

use App\Score\PanneauScore;

class Partie
{
    const DOSSARD_JOUEUR_1 = 1;
    const DOSSARD_JOUEUR_2 = 2;
    private $joueur1;
    private $joueur2;
    private $arbitre;
    private $panneau;

    public function __construct(
        Joueur $joueur1,
        Joueur $joueur2,
        Arbitre $arbitre,
        PanneauScore $panneauScore
    )
    {
        $this->joueur1=$joueur1;
        $this->joueur2=$joueur2;
        $this->arbitre=$arbitre;
        $this->panneau=$panneauScore;
    }

    public function joueManche()
    {
        $resultat=$this->arbitre->juge($this->joueur1->geste(),$this->joueur2->geste());
        if($this->joueur1->geste()->intitule()===$resultat){
            $this->gagnantEst(self::DOSSARD_JOUEUR_1);
        }elseif($this->joueur2->geste()->intitule()===$resultat){
            $this->gagnantEst(self::DOSSARD_JOUEUR_2);
        }
    }

    public function scoreJ1()
    {
        return $this->panneau->scoreJoueur(self::DOSSARD_JOUEUR_1);
    }

    public function scoreJ2()
    {
        return $this->panneau->scoreJoueur(self::DOSSARD_JOUEUR_2);
    }

    private function gagnantEst(int $numeroDossard)
    {
        $this->panneau->incrementeScoreJoueur($numeroDossard);
    }
}