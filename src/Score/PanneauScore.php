<?php


namespace App\Score;


class PanneauScore
{
    private $scores=[];

    public function scoreJoueur($numeroDossard)
    {
        if(isset($this->scores[$numeroDossard])){
            return $this->scores[$numeroDossard];
        }
        return 0;
    }

    public function incrementeScoreJoueur($numeroDossard)
    {
        $this->scores[$numeroDossard]=$this->scoreJoueur($numeroDossard)+1;
    }
}