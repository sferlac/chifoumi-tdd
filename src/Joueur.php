<?php


namespace App;


use App\Gestes\Geste;

interface Joueur
{
    public function geste():Geste;
}