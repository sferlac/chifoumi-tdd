<?php


namespace App\Gestes;


class Feuille extends GesteBase implements Geste
{

    const MARTYR = Geste::GESTE_PIERRE;

    public function intitule(): string
    {
        return Geste::GESTE_FEUILLE;
    }

}