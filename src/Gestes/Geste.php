<?php

namespace App\Gestes;

interface Geste
{
    public const GESTE_FEUILLE = "feuille";
    public const GESTE_PIERRE = "pierre";
    public const GESTE_CISEAUX = "ciseaux";

    public function gagneContre(Geste $geste):bool ;
    public function intitule():string ;
    public function egale(Geste $joueur2):bool ;

}