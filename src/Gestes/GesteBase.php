<?php


namespace App\Gestes;


abstract class GesteBase implements Geste
{


    public function egale(Geste $joueur2): bool
    {
        return $this->intitule()===$joueur2->intitule();
    }
    public function gagneContre(Geste $geste): bool
    {
        if($geste->intitule() === static::MARTYR){
            return true;
        }
        return false;
    }

}