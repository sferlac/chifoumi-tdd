<?php


namespace App\Gestes;


class Ciseaux extends GesteBase implements Geste
{
    const MARTYR = Geste::GESTE_FEUILLE;

    public function intitule(): string
    {
        return Geste::GESTE_CISEAUX;
    }

}