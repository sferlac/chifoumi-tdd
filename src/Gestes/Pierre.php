<?php

namespace App\Gestes;


class Pierre extends GesteBase implements Geste
{
    const MARTYR = Geste::GESTE_CISEAUX;


    public function intitule(): string
    {
        return Geste::GESTE_PIERRE;
    }

}