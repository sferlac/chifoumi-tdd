<?php

namespace App;

use App\Gestes\Geste;

class ArbitreIndifferent extends Arbitre
{

    public function juge(Geste $gesteJoueur1, Geste $gesteJoueur2)
    {
        return static::RESULTAT_EGALITE;
    }
}