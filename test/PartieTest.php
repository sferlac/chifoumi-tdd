<?php

namespace App;

use App\Gestes\Geste;
use App\Score\PanneauScore;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PartieTest extends TestCase
{
    const UN_POINT = 1;
    private $joueur1;
    private $joueur2;
    private $arbitre;
    /** @var MockObject|PanneauScore $panneauScore */
    private $panneauScore;

    public function setUp(): void
    {
        $this->joueur1 = $this->createMock(Joueur::class);
        $this->joueur2 = $this->createMock(Joueur::class);
        $this->arbitre = $this->createMock(Arbitre::class);
        $this->panneauScore = $this->createMock(PanneauScore::class);
    }

   public function test_jouer_une_manche_sollicite_un_arbitrage(){
        $this->arbitre->expects($this->once())->method("juge");
        $partie=new Partie($this->joueur1, $this->joueur2,$this->arbitre, $this->panneauScore);
        $partie->joueManche();
    }
    public function test_si_joueur1_gagne_son_score_augmente(){
        $dossardJoueurGagnant = 1;
        $this->panneauScore->expects($this->once())->method("incrementeScoreJoueur")->with($dossardJoueurGagnant);
        $this->joueMancheDontGagnantEst($dossardJoueurGagnant);
    }
    public function test_si_joueur2_gagne_son_score_augmente(){
        $dossardJoueurGagnant = 2;
        $this->panneauScore->expects($this->once())->method("incrementeScoreJoueur")->with($dossardJoueurGagnant);
        $this->joueMancheDontGagnantEst($dossardJoueurGagnant);
    }
    public function test_si_egalite_scores_sont_inchanges(){
        $partie = $this->partieDontArbitreDonneEgalite();
        $scoreInitialJ1=$partie->scoreJ1();
        $scoreInitialJ2=$partie->scoreJ2();
        $partie->joueManche();
        $this->assertEquals($scoreInitialJ1,$partie->scoreJ1());
        $this->assertEquals($scoreInitialJ2,$partie->scoreJ2());
    }

    public function partieDontArbitreEstAcquisA(int $dossardJoueurGagnant): Partie
    {
        $joueurQuiVaGagner = $this->createMock(Joueur::class);
        $unGeste = $this->createMock(Geste::class);
        $unGeste->method("intitule")->willReturn("Dinosaure");
        $joueurQuiVaGagner->method("geste")->willReturn($unGeste);
        $joueurQuiVaPerdre = $this->createMock(Joueur::class);
        $arbitre = new ArbitreAcquisAJoueur($dossardJoueurGagnant);
        $partie = new Partie($joueurQuiVaPerdre, $joueurQuiVaGagner, $arbitre, $this->panneauScore);
        return $partie;
    }
    public function partieDontArbitreDonneEgalite(): Partie
    {
        return new Partie($this->joueur1, $this->joueur2, new ArbitreIndifferent(),$this->panneauScore);
    }

    private function joueMancheDontGagnantEst(int $dossardJoueurGagnant): void
    {
        $partie = $this->partieDontArbitreEstAcquisA($dossardJoueurGagnant);
        $partie->joueManche();
    }
}

