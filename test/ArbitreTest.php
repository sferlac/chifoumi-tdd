<?php

namespace App;

use App\Gestes\Ciseaux;
use App\Gestes\Feuille;
use App\Gestes\Geste;
use App\Gestes\Pierre;
use PHPUnit\Framework\TestCase;

class ArbitreTest extends TestCase
{
    /** @var Arbitre $arbitre */
    private $arbitre;

    public function setUp(): void
    {
        $this->arbitre = new Arbitre();
    }

    public function test_feuille_contre_feuille_donne_egalite()
    {
        $this->assertEquals(
            Arbitre::RESULTAT_EGALITE,
            $this->arbitre->juge(new Feuille(), new Feuille())
        );
    }
    public function test_feuille_contre_pierre_gagne()
    {
        $this->assertEquals(
            Geste::GESTE_FEUILLE,
            $this->arbitre->juge(new Feuille(), new Pierre())
        );
    }
    public function test_feuille_contre_ciseaux_perd()
    {
        $this->assertEquals(
            Geste::GESTE_CISEAUX,
            $this->arbitre->juge(new Feuille(), new Ciseaux())
        );
    }
    public function test_ciseaux_contre_pierre_perd()
    {
        $this->assertEquals(
            Geste::GESTE_PIERRE,
            $this->arbitre->juge(new Ciseaux(), new Pierre())
        );
    }

}
