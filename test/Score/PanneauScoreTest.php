<?php

namespace App\Score;

use PHPUnit\Framework\TestCase;

class PanneauScoreTest extends TestCase
{
    public function test_panneau_expose_score(){
        $panneau=new PanneauScore();
        $this->assertEquals(0,$panneau->scoreJoueur(1));
    }
    public function test_panneau_sait_incrementer_score(){
        $panneau=new PanneauScore();
        $panneau->incrementeScoreJoueur(2);
        $this->assertEquals(1,$panneau->scoreJoueur(2));
    }

}
