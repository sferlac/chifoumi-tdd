<?php


namespace App;


use App\Gestes\Geste;

class ArbitreAcquisAJoueur extends Arbitre
{
    private $dossardJoueurGagnant;

    public function __construct($dossardJoueurGagnant)
    {
        $this->dossardJoueurGagnant=$dossardJoueurGagnant;
    }

    public function juge(Geste $gesteJoueur1, Geste $gesteJoueur2)
    {
        if($this->dossardJoueurGagnant===Partie::DOSSARD_JOUEUR_1){
            return $gesteJoueur1->intitule();
        }else{
            return $gesteJoueur2->intitule();
        }
    }
}